'use strict';

describe('The search feature', function () {

    beforeEach(function () {
        browser.get('http://localhost:8080');
    });

    it('should displays the no result message', function () {

        element(by.model('search')).sendKeys('anything');

        expect(element(by.binding('search')).getText()).toEqual('No contact for "anything"');
    });

    it('should displays Bruce Wayne when search "Wayne"', function () {

        element(by.model('search')).sendKeys('Wayne');

        expect(element(by.binding('search')).getText()).toEqual('One contact for "Wayne"');

        var contactRows = element.all(by.repeater('contact in contacts'));
        contactRows.each(function (element) {
            expect(element.getTagName()).toBe('li');
            expect(element.getText()).toMatch(/WAYNE/);
        });
    });

});
