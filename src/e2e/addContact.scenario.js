'use strict';

describe('The contact form', function () {

    beforeEach(function () {
        browser.get('http://localhost:8080');
    });

    it('add a new contact', function () {

        element.all(by.className('contact-card')).count().then(function (nbContact) {
            browser.get('http://localhost:8080/#/edit');

            element(by.model('contact.firstName')).sendKeys('Clark');
            element(by.model('contact.lastName')).sendKeys('Kent');
            element(by.model('contact.phone')).sendKeys('555-AZERTY');

            element(by.className('btn-primary')).click();
            expect(element.all(by.className('contact-card')).count()).toBe(nbContact + 1);
        });

    });

});
