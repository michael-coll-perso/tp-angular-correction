(function () {
    'use strict';

    angular.module('zenContact', [
        'zenContact.sections',
        'zenContact.core',
        'zenContact.components'
    ]);

})();
