(function () {
    'use strict';

    angular
        .module('zenContact.sections.edit', [
            'zenContact.core'
        ])
        .controller('ContactEditController', ['$scope', '$routeParams', '$location', '$http', 'contactService', 'Contact', 'authenticator',
            function ($scope, $routeParams, $location, $http, contactService, Contact, authenticator) {

                // contactService.getContactById($routeParams.id, function (contact) {
                //     $scope.contact = contact;
                // });
                //
                // $scope.saveContact = function (contact) {
                //     contactService.saveContact(contact, function (data, err) {
                //         if (!err) {
                //             $location.path('/list');
                //         } else {
                //             console.error(err);
                //         }
                //     });
                // };

                if ($routeParams.id || $routeParams.id === 0) {
                    $scope.contact = Contact.get({
                        id: $routeParams.id
                    });
                } else {
                    $scope.contact = new Contact();
                }

                $scope.saveContact = function (contact) {
                    if (contact.id || contact.id === 0) {
                        contact.$update(function () {
                            $location.path('/list');
                        });
                    } else {
                        Contact.save(contact, function () {
                            $location.path('/list');
                        });
                    }
                };

                $scope.deleteContact = function (contact) {
                    if (contact.id || contact.id === 0) {
                        $http.defaults.headers.delete = {
                            'Auth-Token': authenticator.token()
                        };
                        contact.$delete(function () {
                            $location.path('/list');
                        });
                    }
                };

            }
        ]);

})();
