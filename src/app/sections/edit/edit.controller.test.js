'use strict';

describe('Contact edit controller', function () {
    var $scope;
    var $httpBackend;
    var $controller;

    var contacts = [
        {
            "id": 0,
            "lastName": "Wayne",
            "firstName": "Bruce",
            "address": "Gotham city",
            "phone": "555-BATMAN"
        },
        {
            "id": 1,
            "lastName": "Parker",
            "firstName": "Peter",
            "address": "New York",
            "phone": "555-SPDRMN"
        }
      ];

    var newContact = {
        "lastName": "Stark",
        "firstName": "Tony",
        "address": "Stark tower, New York",
        "phone": "555-IRNMAN"
    };

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.sections.edit'));

    beforeEach(inject(function ($rootScope, _$httpBackend_, _$controller_) {
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
        $controller = _$controller_;
    }));

    it('should load a contact when $routeParams have an id', inject(function () {

        expect($scope.contact).toBeUndefined();
        $httpBackend.expectGET('/rest/contacts/0').respond(contacts[0]);

        $controller('ContactEditController', {
            $scope: $scope,
            $routeParams: {
                id: '0'
            }
        });
        $httpBackend.flush();

        expect($scope.contact).toBeDefined();
        expect($scope.contact.id).toBe(0);
    }));

    it('should create a contact when $routeParams doesn\'t have an id', inject(function () {

        expect($scope.contact).toBeUndefined();

        $controller('ContactEditController', {
            $scope: $scope,
            $routeParams: {}
        });

        expect($scope.contact).toBeDefined();
        expect($scope.contact.id).toBeUndefined();
    }));

    it('should save contact when contact is new', inject(function ($location) {

        expect($scope.saveContact).toBeUndefined();
        expect($scope.contact).toBeUndefined();

        $httpBackend.expectPOST('/rest/contacts')
            .respond(function (method, url, data) {
                var updatedContact = JSON.parse(data);
                updatedContact.id = 12;
                return [200, updatedContact, {}];
            });

        $controller('ContactEditController', {
            $scope: $scope,
            $routeParams: {}
        });

        expect($scope.contact).toBeDefined();
        expect($scope.contact.id).toBeUndefined();

        angular.extend($scope.contact, newContact);

        $scope.saveContact($scope.contact);
        $httpBackend.flush();

        expect($location.path()).toBe('/list');
    }));

    it('should return valid contact when save existing contact', inject(function ($location) {
        $httpBackend.expectGET('/rest/contacts/0').respond(contacts[0]);

        $controller('ContactEditController', {
            $scope: $scope,
            $routeParams: {
                id: '0'
            }
        });
        $httpBackend.flush();

        $httpBackend.expectPUT('/rest/contacts/0')
            .respond(function (method, url, data) {
                return [200, JSON.parse(data), {}];
            });

        $scope.saveContact($scope.contact);

        $httpBackend.flush();

        expect($location.path()).toBe('/list');
    }));

    it('should redirect to list when delete contact', inject(function ($location) {
        $httpBackend.expectGET('/rest/contacts/0').respond(contacts[0]);

        $controller('ContactEditController', {
            $scope: $scope,
            $routeParams: {
                id: '0'
            }
        });
        $httpBackend.flush();

        $httpBackend.expectDELETE('/rest/contacts/0')
            .respond(function () {
                return [200, {}, {}];
            });

        $scope.deleteContact($scope.contact);

        $httpBackend.flush();

        expect($location.path()).toBe('/list');
    }));

    it('should redirect to login when delete contact and server response 401', inject(function ($location) {
        $httpBackend.expectGET('/rest/contacts/0').respond(contacts[0]);

        $controller('ContactEditController', {
            $scope: $scope,
            $routeParams: {
                id: '0'
            }
        });
        $httpBackend.flush();

        $httpBackend.expectDELETE('/rest/contacts/0')
            .respond(function () {
                return [401, {}, {}];
            });

        $scope.deleteContact($scope.contact);

        $httpBackend.flush();

        expect($location.path()).toBe('/login');
    }));

});
