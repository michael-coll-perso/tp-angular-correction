(function () {
    'use strict';

    angular
        .module('zenContact.sections.navbar', [])
        .controller('NavBarController', ['$scope', '$location',
            function ($scope, $location) {

                $scope.isActive = function (path) {
                    return $location.path().indexOf(path) !== -1;
                };

            }
        ]);

})();
