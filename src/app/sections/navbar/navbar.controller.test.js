'use strict';

describe('NavBar controller', function () {
    var $scope;
    var $controller;
    var $location;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.sections.navbar'));

    beforeEach(inject(function ($rootScope, _$controller_, _$location_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $location = _$location_;
    }));

    it('should be active when path is equals to current path', function () {

        $location.path('/list');

        $controller('NavBarController', {
            $scope: $scope
        });

        expect($scope.isActive('/list')).toBeTruthy();
    });

    it('should not be active when path is not equals to current path', function () {

        $location.path('/edit');

        $controller('NavBarController', {
            $scope: $scope
        });

        expect($scope.isActive('/list')).toBeFalsy();
    });

    it('should be active when path is contains into current path', function () {

        $location.path('/edit/0');

        $controller('NavBarController', {
            $scope: $scope
        });

        expect($scope.isActive('/edit')).toBeTruthy();
    });

});
