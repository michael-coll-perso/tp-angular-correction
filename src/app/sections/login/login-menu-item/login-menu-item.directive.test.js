'use strict';

describe('Login Menu Item directive', function () {
    var $compile,
        $scope,
        $location;

    function Response(headers) {
        this._headers = headers || {};
    }
    Response.prototype.headers = function (key) {
        var header;
        if (key) {
            header = this._headers[key];
        }
        return header;
    };

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.sections.login.login-menu-item'));

    beforeEach(inject(function (_$compile_, $rootScope, _$location_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $location = _$location_;

        $scope = $rootScope.$new();
    }));

    it('should hide logout button when not login', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<login-menu-item></login-menu-item>')($scope);

        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.hasClass('ng-hide')).toBeTruthy();
    });

    it('should show logout button when login', inject(function (authenticator) {

        // Compile a piece of HTML containing the directive
        var element = $compile('<login-menu-item></login-menu-item>')($scope);

        authenticator.storeToken(new Response({
            'Auth-Token': 'uuid 01'
        }));

        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.hasClass('ng-hide')).toBeFalsy();
    }));

    it('should hide logout button when login and button click', inject(function (authenticator) {

        // Compile a piece of HTML containing the directive
        var element = $compile('<login-menu-item></login-menu-item>')($scope);

        authenticator.storeToken(new Response({
            'Auth-Token': 'uuid 01'
        }));

        $scope.$apply();
        expect(element.hasClass('ng-hide')).toBeFalsy();

        element.find('a').click();

        expect(element.hasClass('ng-hide')).toBeTruthy();
    }));


    it('should hide logout button when not login and button click', inject(function (authenticator) {

        // Compile a piece of HTML containing the directive
        var element = $compile('<login-menu-item></login-menu-item>')($scope);

        $scope.$apply();
        expect(element.hasClass('ng-hide')).toBeTruthy();

        element.find('a').click();

        expect(element.hasClass('ng-hide')).toBeTruthy();
    }));

});
