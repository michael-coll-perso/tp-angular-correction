(function () {
    'use strict';

    angular
        .module('zenContact.sections.login.login-menu-item', [
            'zenContact.core.security.authenticator'
        ])
        .directive('loginMenuItem', function () {
            return {
                restrict: 'E',
                replace: true,
                template: '<li data-ng-show="loginBtnCtrl.show"><a data-ng-click="loginBtnCtrl.action()">Logout</a></li>',
                scope: false,
                controller: ['$scope', '$location', 'authenticator', function ($scope, $location, authenticator) {

                    var model = this;
                    $scope.authenticator = authenticator;
                    $scope.$watch('authenticator.token()', function (newValue) {
                            model.show = !!newValue;
                    });

                    model.action = function () {
                        if (authenticator.token()) {
                            model.show = false;
                            $location.path('/logout');
                        }
                    };

                }],
                controllerAs: 'loginBtnCtrl'
            };
        });



})();
