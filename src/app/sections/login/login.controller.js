(function () {
    'use strict';

    angular
        .module('zenContact.sections.login', [
            'zenContact.sections.login.login-menu-item',
            'zenContact.core.security.authenticator'
        ])
        .controller('LoginController', ['$scope', '$http', 'authenticator',
            function ($scope, $http, authenticator) {

                $scope.user = {};

                $scope.login = function (user) {
                    $http.post('/rest/login/' + user.name).success(authenticator.maybeRedirect);
                };
            }

        ]);



})();
