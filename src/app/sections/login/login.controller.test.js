'use strict';

describe('Login Controller', function () {
    var $scope,
        $httpBackend,
        $location;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.sections.login'));

    beforeEach(inject(function ($rootScope, _$location_, _$httpBackend_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $httpBackend = _$httpBackend_;
        $location = _$location_;
        $scope = $rootScope.$new();
    }));

    it('should POST user name and redirect to "/list" when login', inject(function ($controller) {

        $httpBackend.expectPOST().respond({});

        $controller('LoginController', {
            $scope: $scope
        });

        $scope.user.name = 'foobar';

        $scope.login($scope.user);

        $httpBackend.flush();

        // Check that the compiled element contains the templated content
        expect($location.path()).toBe('/list');
    }));

    it('should POST user name and redirect to the last path when login', inject(function ($controller, authenticator) {

        $httpBackend.expectPOST().respond({});
        $location.path('/edit');
        authenticator.redirectToLogin();

        $controller('LoginController', {
            $scope: $scope
        });

        $scope.user.name = 'foobar';

        $scope.login($scope.user);

        $httpBackend.flush();

        // Check that the compiled element contains the templated content
        expect($location.path()).toBe('/edit');
    }));

});
