(function () {
    'use strict';

    angular
        .module('zenContact.sections.logout', [
            'zenContact.core.security.authenticator'
        ])
        .controller('LogoutController', ['authenticator', '$location',
            function (authenticator, $location) {

                authenticator.logout();
                $location.path('/list');
            }

        ]);

})();
