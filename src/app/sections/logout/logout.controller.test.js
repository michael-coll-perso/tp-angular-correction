'use strict';

describe('Logout Controller', function () {
    var $scope,
        $location;

    function Response(headers) {
        this._headers = headers || {};
    }
    Response.prototype.headers = function (key) {
        var header;
        if (key) {
            header = this._headers[key];
        }
        return header;
    };

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.sections.logout'));

    beforeEach(inject(function ($rootScope, _$location_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $location = _$location_;
        $scope = $rootScope.$new();
    }));

    it('should logout and redirect to "/list" when call Logout', inject(function ($controller, authenticator) {

        authenticator.storeToken(new Response({
            'Auth-Token': 'uuid 02'
        }));

        expect(authenticator.token()).toBeDefined();

        $controller('LogoutController', {
            $scope: $scope
        });

        // Check that the compiled element contains the templated content
        expect($location.path()).toBe('/list');
        expect(authenticator.token()).toBeUndefined();
    }));

});
