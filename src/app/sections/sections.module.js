(function () {
    'use strict';

    angular.module('zenContact.sections', [
        'ngRoute',
        'ngMessages',
        'ngAnimate',
        'zenContact.sections.edit',
        'zenContact.sections.list',
        'zenContact.sections.login',
        'zenContact.sections.logout',
        'zenContact.sections.navbar'
    ]);

})();
