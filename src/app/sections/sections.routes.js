(function () {
    'use strict';

    angular
        .module('zenContact.sections')
        .config(['$routeProvider',
            function ($routeProvider) {

                $routeProvider
                    .when('/list', {
                        templateUrl: 'app/sections/list/list.html',
                        controller: 'ContactListController'
                    })
                    .when('/edit', {
                        templateUrl: 'app/sections/edit/edit.html',
                        controller: 'ContactEditController'
                    })
                    .when('/edit/:id', {
                        templateUrl: 'app/sections/edit/edit.html',
                        controller: 'ContactEditController'
                    })
                    .when('/login', {
                        templateUrl: 'app/sections/login/login.html',
                        controller: 'LoginController'
                    })
                    .when('/logout', {
                        template: '<div></div>',
                        controller: 'LogoutController'
                    });

                $routeProvider.otherwise('/list');
            }

        ]);

})();
