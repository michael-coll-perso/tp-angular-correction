(function () {
    'use strict';

    angular
        .module('zenContact.sections.list', [
            'zenContact.core'
        ])
        .controller('ContactListController', ['$scope', 'contactService', 'Contact',
            function ($scope, contactService, Contact) {

                //            contactService.getAllContacts(function (contacts) {
                //                $scope.contacts = contacts;
                //            });

                $scope.contacts = Contact.query();

                $scope.nameFilter = function (contact) {
                    if (!$scope.search) {
                        return true;
                    }
                    return contact.firstName.match(new RegExp($scope.search, 'i')) || contact.lastName.match(new RegExp($scope.search, 'i'));
                };
            }

        ]);


})();
