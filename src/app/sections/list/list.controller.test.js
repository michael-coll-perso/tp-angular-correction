'use strict';

describe('Contact list controller', function () {
    var $scope;
    var $httpBackend;

    var contacts = [
        {
            "id": 0,
            "lastName": "Wayne",
            "firstName": "Bruce",
            "address": "Gotham city",
            "phone": "555-BATMAN"
        },
        {
            "id": 1,
            "lastName": "Parker",
            "firstName": "Peter",
            "address": "New York",
            "phone": "555-SPDRMN"
        }
      ];

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.sections.list'));

    beforeEach(inject(function ($rootScope, _$httpBackend_) {
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
        $httpBackend.whenGET().respond(contacts);
    }));

    it('should define the name filter', inject(function ($controller) {
        expect($scope.nameFilter).toBeUndefined();

        $controller('ContactListController', {
            $scope: $scope
        });

        expect(angular.isFunction($scope.nameFilter)).toBeTruthy();
    }));

    it('should return true when search "Bru" for Bruce Wayne', inject(function ($controller) {

        $controller('ContactListController', {
            $scope: $scope
        });

        $httpBackend.flush();

        $scope.search = 'Bru';

        expect($scope.nameFilter(contacts[0])).toBeTruthy();
    }));

    it('should return false when search "Par" for Bruce Wayne', inject(function ($controller) {

        $controller('ContactListController', {
            $scope: $scope
        });

        $httpBackend.flush();

        $scope.search = 'Par';

        expect($scope.nameFilter(contacts[0])).toBeFalsy();
    }));

    it('should return true when search nothing', inject(function ($controller) {

        $controller('ContactListController', {
            $scope: $scope
        });

        $httpBackend.flush();

        $scope.search = null;

        expect($scope.nameFilter(contacts[0])).toBeTruthy();
    }));

    it('should received 2 mocked contact', inject(function ($controller) {
        expect($scope.contacts).toBeUndefined();

        $controller('ContactListController', {
            $scope: $scope
        });

        expect($scope.contacts).toBeDefined();

        $httpBackend.flush();

        expect($scope.contacts.length).toBe(2);
    }));

});
