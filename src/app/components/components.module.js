(function () {
    'use strict';

    angular.module('zenContact.components', [
        'zenContact.components.filters',
        'zenContact.components.directives'
    ]);

})();
