(function () {
    'use strict';

    angular.module('zenContact.components.filters', [
        'ui.unique',
        'zenContact.components.filters.by-name',
        'zenContact.components.filters.fuzzy'
    ]);

})();
