(function () {
    'use strict';

    angular
        .module('zenContact.components.filters.fuzzy', [])
        .filter('fuzzy', ['$window', function ($window) {
            return function (contacts, search, threshold) {
                if (!search || !contacts) {
                    return contacts || [];
                }

                var fuse = new $window.Fuse(contacts, {
                    keys: ['firstName', 'lastName'],
                    threshold: threshold
                });

                return fuse.search(search);
            };
        }]);

})();
