'use strict';

describe('Fuzzy filter', function () {
    var $scope;
    var fuzzyFilter;

    var contacts = [
        {
            "id": 0,
            "lastName": "Wayne",
            "firstName": "Bruce",
            "address": "Gotham city",
            "phone": "555-BATMAN"
        },
        {
            "id": 1,
            "lastName": "Parker",
            "firstName": "Peter",
            "address": "New York",
            "phone": "555-SPDRMN"
        }
      ];

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.filters.fuzzy'));

    beforeEach(inject(function ($rootScope, _fuzzyFilter_) {
        $scope = $rootScope.$new();
        fuzzyFilter = _fuzzyFilter_;

        /*
         * Si on connait le nom du filtre dont on as besoin au coding time, utiliser l'injection de filtre.
         * Si on ne le connait qu'au runtime (il est dans une string), alors utiliser $filter.
         */
    }));

    it('should find Bruce Wayne when search "bt"', function () {

        var result = fuzzyFilter(contacts, 'bt', 0.5);

        expect(result).toBeDefined();
        expect(result.length).toBe(1);
        expect(result).toEqual([contacts[0]]);
    });


    it('should find nothing when search "555"', function () {

        var result = fuzzyFilter(contacts, '55');

        expect(result).toBeDefined();
        expect(result.length).toBe(0);
    });

    it('should find all when search ""', inject(function () {

        var result = fuzzyFilter(contacts, '');

        expect(result).toBeDefined();
        expect(result.length).toBe(2);
    }));

    it('should return an empty array when search in an null array', inject(function () {

        var result = fuzzyFilter(null, 't');

        expect(result).toBeDefined();
        expect(result.length).toBe(0);
    }));
});
