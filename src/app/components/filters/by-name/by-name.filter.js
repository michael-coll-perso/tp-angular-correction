(function () {
    'use strict';

    angular
        .module('zenContact.components.filters.by-name', [])
        .filter('byName', ['filterFilter', function (filterFilter) {
            return function (contacts, search) {
                return filterFilter(contacts, function (contact) {
                    return !search || contact.firstName.match(new RegExp(search, 'i')) || contact.lastName.match(new RegExp(search, 'i'));
                });
            };
        }]);

})();
