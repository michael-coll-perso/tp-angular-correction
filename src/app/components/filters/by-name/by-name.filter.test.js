'use strict';

describe('By name filter', function () {
    var $scope;
    var byNameFilter;

    var contacts = [
        {
            "id": 0,
            "lastName": "Wayne",
            "firstName": "Bruce",
            "address": "Gotham city",
            "phone": "555-BATMAN"
        },
        {
            "id": 1,
            "lastName": "Parker",
            "firstName": "Peter",
            "address": "New York",
            "phone": "555-SPDRMN"
        }
      ];

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.filters.by-name'));

    beforeEach(inject(function ($rootScope, _byNameFilter_) {
        $scope = $rootScope.$new();
        byNameFilter = _byNameFilter_;

        /*
         * Si on connait le nom du filtre dont on as besoin au coding time, utiliser l'injection de filtre.
         * Si on ne le connait qu'au runtime (il est dans une string), alors utiliser $filter.
         */
    }));

    it('should find Bruce Wayne when search "b"', inject(function () {

        var result = byNameFilter(contacts, 'b');

        expect(result).toBeDefined();
        expect(result.length).toBe(1);
        expect(result).toEqual([contacts[0]]);
    }));

    it('should find nothing when search "555"', inject(function () {

        var result = byNameFilter(contacts, '55');

        expect(result).toBeDefined();
        expect(result.length).toBe(0);
    }));

    it('should find all when search ""', inject(function () {

        var result = byNameFilter(contacts, '');

        expect(result).toBeDefined();
        expect(result.length).toBe(2);
    }));

    it('should return null when search in an null array', inject(function () {

        var result = byNameFilter(null, 't');

        expect(result).toBeNull();
    }));

});
