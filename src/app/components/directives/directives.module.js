(function () {
    'use strict';

    angular.module('zenContact.components.directives', [
        'zenContact.components.directives.auto-height',
        'zenContact.components.directives.auto-popup',
        'zenContact.components.directives.markdown',
        'zenContact.components.directives.markdown1',
        'zenContact.components.directives.markdown2',
        'zenContact.components.directives.active-menu-item',
        'zenContact.components.directives.menu-item',
        'zenContact.components.directives.routing-menu'
    ]);

})();
