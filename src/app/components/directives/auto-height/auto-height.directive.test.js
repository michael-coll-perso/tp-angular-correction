'use strict';

describe('Auto-Height directive', function () {
    var $compile,
        $rootScope;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.directives.auto-height'));

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $rootScope = _$rootScope_;
    }));

    it('should resize element heigth when window height equals 300', inject(function ($window) {

        var scope = $rootScope.$new();
        var fixedSize = 60;

        // Compile a piece of HTML containing the directive
        var element = $compile('<div data-auto-height="' + fixedSize + '">test</div>')(scope);

        // Check that the compiled element contains the templated content
        expect(element.height()).toEqual($window.innerHeight - fixedSize);
    }));
});
