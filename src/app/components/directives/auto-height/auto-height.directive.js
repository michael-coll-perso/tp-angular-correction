(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.auto-height', [])
        .directive('autoHeight', ['$window',
            function autoHeight($window) {
                return function ($scope, element, attributes) {

                    element.css('overflow', 'auto');

                    function changeHeight() {
                        var height = $window.innerHeight - attributes.autoHeight;
                        element.height(height);
                    }

                    changeHeight();
                    angular.element($window).resize(changeHeight);

                    $scope.$on('$destroy', function () {
                        angular.element($window).off('resize');
                    });

                };
            }
        ]);

})();
