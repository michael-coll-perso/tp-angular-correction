(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.auto-popup', [])
        .directive('autoPopup', ['$http',
            function ($http) {
                var modal = angular.element(
                    '<div class="modal fade" tabindex="-1">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-body"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
                var modalContent = modal.find('.modal-body');

                return function (scope, element, attributes) {
                    element.click(function () {
                        $http.get(attributes.autoPopup).success(function (content) {
                            modalContent.html(content);
                            modal.modal('show');
                        });
                    });

                    scope.$on('$destroy', function () {
                        element.off('click');
                    });
                };
            }
        ]);

})();
