'use strict';

describe('Auto-popup directive', function () {
    var $compile,
        $rootScope,
        $httpBackend;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.directives.auto-popup'));

    beforeEach(inject(function (_$compile_, _$rootScope_, _$httpBackend_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
    }));

    it('should create popup when click', inject(function () {

        $httpBackend.when('GET').respond('<div>Foobar</div>');

        var scope = $rootScope.$new();

        // Compile a piece of HTML containing the directive
        var element = $compile('<div data-auto-popup="/toto.html">test1</div>')(scope);

        element.click();

        $httpBackend.flush();

        var wholePage = document.documentElement.outerHTML;
        var modalBody = angular.element(wholePage).find('.modal-body');

        expect(modalBody).toBeDefined();

        // Check that the compiled element contains the templated content
        expect(modalBody.html()).toEqual('<div>Foobar</div>');
    }));

});
