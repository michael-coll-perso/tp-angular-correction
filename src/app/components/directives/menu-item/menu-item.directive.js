(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.menu-item', [])
        .directive('menuItem', ['$location', function ($location) {
            return {
                restrict: 'E',
                replace: true,
                template: '<li ng-class="{active: active}"><a href={{link}}>{{label}}</a></li>',
                //                template: '<li ng-class="{active: menuItemCtrl.activeClass()}"><a href={{link}}>{{label}}</a></li>',
                scope: {
                    link: '@',
                    label: '@'
                },
                controller: ['$scope', function ($scope) {
                    var path = $scope.link.substring(1);

                    //                    var count = 0;
                    $scope.location = $location;

                    //                    this.activeClass = function () {
                    //                        console.log('menuItem: ', count++);
                    //                        return location.path() === path;
                    //                    };

                    $scope.$watch('location.path()', function (newPath) {
                        //                        console.log('menuItem: ', count++);
                        $scope.active = path === newPath;
                    });
                }],
                controllerAs: 'menuItemCtrl'
            };
    }]);
})();
