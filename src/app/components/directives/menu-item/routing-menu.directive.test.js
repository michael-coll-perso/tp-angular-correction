'use strict';

describe('Routing menu directive', function () {
    var $compile,
        $scope,
        $location;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.directives.routing-menu'));

    beforeEach(inject(function (_$compile_, $rootScope, _$location_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $location = _$location_;

        $scope = $rootScope.$new();
    }));

    it('should have "activate" class anchor with "#/edit" href', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<ul routing-menu="active"><li><a href="#/list" class="routing-item">A</a></li><li><a href="#/edit" class="routing-item">B</a></li>')($scope);

        $location.path('/edit');
        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.find('.active .routing-item').attr('href')).toBe('#/edit');
    });

    it('should have "activate" class anchor with "#/list" href', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<ul routing-menu="active"><li><a href="#/list" class="routing-item">A</a></li><li><a href="#/edit" class="routing-item">B</a></li>')($scope);

        $location.path('/list');
        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.find('.active .routing-item').attr('href')).toBe('#/list');
    });

    it('should have no anchor with "activate" class', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<ul routing-menu="active"><li><a href="#/list" class="routing-item">A</a></li><li><a href="#/edit" class="routing-item">B</a></li>')($scope);

        $location.path('/toto');
        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.find('.active .routing-item').attr('href')).toBeUndefined();
    });

});
