(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.active-menu-item', [])
        .directive('activeMenuItem', ['$location', function ($location) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {

                    var clazz = attrs.activeMenuItem;

                    var link = element.find('a');
                    var path = link.attr('href').substring(1); //hack because path does bot return including hashbang

                    scope.location = $location;

                    scope.$watch('location.path()', function (newPath) {
                        if (path === newPath) {
                            element.addClass(clazz);
                        } else {
                            element.removeClass(clazz);
                        }
                    });
                }
            };
    }]);
})();
