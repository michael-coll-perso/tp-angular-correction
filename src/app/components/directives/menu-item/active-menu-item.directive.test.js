'use strict';

describe('Active menu item directive', function () {
    var $compile,
        $scope,
        $location;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.directives.active-menu-item'));

    beforeEach(inject(function (_$compile_, $rootScope, _$location_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $location = _$location_;

        $scope = $rootScope.$new();
    }));

    it('should have "activate" class when path == href', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<li active-menu-item="active"><a href="#/edit">Add new contact</a></li>')($scope);

        $location.path('/edit');
        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.hasClass('active')).toBeTruthy();
    });

    it('should haven\'t "activate" class when path != href', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<li active-menu-item="active"><a href="#/edit">Add new contact</a></li>')($scope);

        $location.path('/list');
        $scope.$apply();

        // Check that the compiled element contains the templated content
        expect(element.hasClass('active')).toBeFalsy();
    });

});
