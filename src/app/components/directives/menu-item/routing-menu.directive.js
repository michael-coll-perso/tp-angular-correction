(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.routing-menu', [])
        .directive('routingMenu', ['$location', function ($location) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {

                    var clazz = attrs.routingMenu;

                    scope.location = $location;

                    scope.$watch('location.path()', function (newPath) {

                        var allLinks = element.find('.routing-item');

                        allLinks.each(function (index, link) {

                            var eltLink = angular.element(link);
                            var path = eltLink.attr('href').substring(1); // hack car le chemin retourné inclut le hash
                            if (path === newPath) {
                                eltLink.parent().addClass(clazz);
                            } else {
                                eltLink.parent().removeClass(clazz);
                            }

                        });

                    });

                }
            };
    }]);
})();
