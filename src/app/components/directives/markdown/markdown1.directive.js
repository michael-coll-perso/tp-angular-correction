(function () {
    'use strict';
    // https://gist.github.com/mjeanroy/9747993d24126d082185

    angular
        .module('zenContact.components.directives.markdown1', [])
        .directive('markdown1', ['$sce', '$window',
            function ($sce, $window) {

                return {
                    require: 'ngModel',
                    restrict: 'A',
                    template: '<textarea></textarea><div>result : <div class="well md"></div></div>',
                    link: function (scope, element, attributes, ngModel) {
                        var $textarea = element.find('textarea');
                        var $md = element.find('.md');

                        $textarea.on('keyup', function () {
                            scope.$apply(function () {
                                var value = $textarea.val();
                                ngModel.$setViewValue(value || '');
                            });
                        });

                        var converter = new $window.Showdown.converter();
                        scope.$watch(attributes.ngModel, function (newValue) {
                            var html = '';
                            if (newValue) {
                                html = $sce.trustAsHtml(converter.makeHtml(newValue)).toString();
                            }

                            $md.html(html);
                        });

                        scope.$on('$destroy', function () {
                            $textarea.off();
                        });
                    }
                };
            }
        ]);
})();
