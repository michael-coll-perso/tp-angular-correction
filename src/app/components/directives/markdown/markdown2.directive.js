(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.markdown2', [])
        .directive('markdown2', ['$sce', '$window',
            function ($sce, $window) {
                return {
                    restrict: 'A',
                    template: '<textarea class="form-control" data-ng-model="source"></textarea>' +
                        '<div data-ng-if="mdCtrl.markdown">' +
                        '    <div class="well" data-ng-bind-html="mdCtrl.markdown"></div>' +
                        '</div>',
                    scope: {
                        source: '=ngModel'
                    },
                    controller: ['$scope', function ($scope) {
                        var converter = new $window.Showdown.converter();
                        var model = this;
                        $scope.$watch('source', function (newSource) {
                            model.markdown = $scope.source && $sce.trustAsHtml(converter.makeHtml(newSource));
                        });
                    }],
                    controllerAs: 'mdCtrl'
                };
            }
        ]);

})();
