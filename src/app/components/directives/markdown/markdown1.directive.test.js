'use strict';

describe('Markdown1 directive', function () {
    var $compile,
        $scope;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.directives.markdown1'));

    beforeEach(inject(function (_$compile_, $rootScope) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $scope = $rootScope.$new();
    }));

    it('should create H1 when text start with "# "', function () {

        // Compile a piece of HTML containing the directive
        var element = $compile('<div markdown-1 data-ng-model="text"></div>')($scope);

        element.find('textarea').html('# Foobar');
        element.find('textarea').keyup();

        var md = element.find('.md')[0].innerHTML;

        // Check that the compiled element contains the templated content
        expect(md).toEqual('<h1 id="foobar">Foobar</h1>');
    });

    it('should clean result when text replace by empty String', function () {


        // Compile a piece of HTML containing the directive
        var element = $compile('<div markdown-1 data-ng-model="text"></div>')($scope);

        element.find('textarea').html('# Foobar');
        element.find('textarea').keyup();


        element.find('textarea').html('');
        element.find('textarea').keyup();

        var md = element.find('.md')[0].innerHTML;

        // Check that the compiled element contains the templated content
        expect(md).toEqual('');
    });

});
