(function () {
    'use strict';

    angular
        .module('zenContact.components.directives.markdown', [])
        .directive('markdown', ['$sce', '$window',
            function ($sce, $window) {

                return {
                    restrict: 'A',
                    template: '<textarea class="form-control" data-ng-model="source"></textarea>' +
                        '<div data-ng-if="source">' +
                        '    <div class="well" data-ng-bind-html="markdown"></div>' +
                        '</div>',
                    scope: {
                        source: '=ngModel'
                    },
                    link: function (scope) {
                        var converter = new $window.Showdown.converter();
                        scope.$watch('source', function (newValue) {
                            if (newValue) {
                                scope.markdown = $sce.trustAsHtml(converter.makeHtml(newValue));
                            } else {
                                scope.markdown = '';
                            }
                        });
                    }
                };
            }
        ]);

})();
