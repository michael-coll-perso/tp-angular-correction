'use strict';

describe('Markdown directive', function () {
    var $compile,
        $scope;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.components.directives.markdown'));

    beforeEach(inject(function (_$compile_, $rootScope) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $scope = $rootScope.$new();
    }));

    it('should create H1 when text start with "# "', function () {

        $scope.text = '# Foobar';

        // Compile a piece of HTML containing the directive
        var element = $compile('<div markdown data-ng-model="text"></div>')($scope);

        $scope.$apply();

        var md = element.find('.ng-binding')[0].innerHTML;

        // Check that the compiled element contains the templated content
        expect(md).toEqual('<h1 id="foobar">Foobar</h1>');
    });

    it('should clean result when text replace by empty String', function () {

        $scope.text = '# Foobar';

        // Compile a piece of HTML containing the directive
        var element = $compile('<div markdown data-ng-model="text"></div>')($scope);

        $scope.text = '';
        $scope.$apply();

        var md = element.find('.ng-binding')[0];

        // Check that the compiled element contains the templated content
        expect(md).toBeFalsy();
    });

});
