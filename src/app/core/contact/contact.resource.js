(function () {
    'use strict';

    angular
        .module('zenContact.core.contact')
        .factory('Contact', ['$resource', function ($resource) {
            return $resource('/rest/contacts/:id', { id: '@id' }, {
                update: {
                    method: 'PUT',
                    params: {
                        id: '@id'
                    }
                }
            });
        }]);

})();
