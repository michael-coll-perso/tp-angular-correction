'use strict';

describe('Contact service', function () {
    var $httpBackend;
    var contactService;

    var contacts = [
        {
            "id": 0,
            "lastName": "Wayne",
            "firstName": "Bruce",
            "address": "Gotham city",
            "phone": "555-BATMAN"
        },
        {
            "id": 1,
            "lastName": "Parker",
            "firstName": "Peter",
            "address": "New York",
            "phone": "555-SPDRMN"
        }
      ];

    var newContact = {
        "lastName": "Stark",
        "firstName": "Tony",
        "address": "Stark tower, New York",
        "phone": "555-IRNMAN"
    };

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.core.contact'));

    beforeEach(inject(function ($rootScope, _$httpBackend_, _contactService_) {
        $httpBackend = _$httpBackend_;
        contactService = _contactService_;
    }));

    it('should return all contacts list', function () {
        $httpBackend.expectGET('/rest/contacts').respond(contacts);

        contactService.getAllContacts(callback);

        $httpBackend.flush();

        function callback(data) {
            expect(data.length).toBe(2);
        }
    });

    it('should return Bruce Wayne when call with id "0"', function () {
        $httpBackend.expectGET('/rest/contacts/0').respond(contacts[0]);

        contactService.getContactById(0, callback);

        $httpBackend.flush();

        function callback(data) {
            expect(data).toEqual(contacts[0]);
        }
    });

    it('should return empty object when call with unknown id', function () {
        $httpBackend.expectGET().respond(500, '');

        contactService.getContactById(15, callback);

        $httpBackend.flush();

        function callback(data) {
            expect(data).toEqual({});
        }
    });

    it('should add contact id when save new contact', function () {
        $httpBackend.expectPOST('/rest/contacts')
            .respond(function (method, url, data, headers) {
                var updatedContact = JSON.parse(data);
                updatedContact.id = 12;
                return [200, updatedContact, {}];
            });

        contactService.saveContact(newContact, callback);

        $httpBackend.flush();

        function callback(data) {
            var conctact = angular.copy(newContact);
            conctact.id = 12;
            expect(data).toEqual(conctact);
        }
    });

    it('should return valid contact when save existing contact', function () {
        $httpBackend.expectPUT('/rest/contacts/0')
            .respond(function (method, url, data, headers) {
                return [200, JSON.parse(data), {}];
            });

        contactService.saveContact(contacts[0], callback);

        $httpBackend.flush();

        function callback(data) {
            expect(data).toEqual(contacts[0]);
        }
    });

});
