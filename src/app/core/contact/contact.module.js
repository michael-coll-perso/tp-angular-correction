(function () {
    'use strict';

    angular
        .module('zenContact.core.contact', [
            'ngResource'
        ]);

})();
