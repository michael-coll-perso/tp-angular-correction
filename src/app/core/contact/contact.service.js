(function () {
    'use strict';

    angular
        .module('zenContact.core.contact')
        .factory('contactService', ['$http',
            function ($http) {

                var instance = {};

                instance.getAllContacts = function (callback) {
                    $http.get('/rest/contacts').success(callback);
                };

                instance.getContactById = function (id, callback) {
                    $http.get('/rest/contacts/' + id)
                        .success(function (contact) {
                            callback(contact);
                        }).error(function () {
                            callback({});
                        });
                };

                instance.saveContact = function (contact, callback) {
                    if (angular.isDefined(contact.id)) {
                        $http.put('/rest/contacts/' + contact.id, contact)
                            .success(function (data) {
                                callback(data);
                            }).error(function () {
                                callback(null, 'Error');
                            });
                    } else {
                        $http.post('/rest/contacts', contact)
                            .success(function (data) {
                                callback(data);
                            }).error(function () {
                                callback(null, 'Error');
                            });
                    }
                };

                return instance;
            }
        ]);
})();
