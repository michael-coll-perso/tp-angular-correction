(function () {
    'use strict';

    angular
        .module('zenContact.core.security.config', [
            'zenContact.core.security.authenticator'
        ])
        .config(['$httpProvider',
            function ($httpProvider) {

                $httpProvider.interceptors.push(['$q', 'authenticator',
                    function ($q, authenticator) {
                        return {
                            response: function (response) {
                                authenticator.storeToken(response);
                                return response || $q.when(response);
                            },
                            responseError: function (rejection) {
                                if (rejection.status === 401) {
                                    authenticator.redirectToLogin();
                                }
                                return $q.reject(rejection);
                            }
                        };
                    }
                ]);
            }
        ]);
})();
