(function () {
    'use strict';

    angular.module('zenContact.core.security', [
        'zenContact.core.security.config',
        'zenContact.core.security.authenticator'
    ]);

})();
