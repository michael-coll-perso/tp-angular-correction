'use strict';

describe('Authenticator service', function () {
    var authenticator;
    var $location;
    var $cookieStore;
    var tokenId = 'Auth-Token';
    var wrongTokenId = 'Token';

    function Response(headers) {
        this._headers = headers || {};
    }
    Response.prototype.headers = function (key) {
        var header;
        if (key) {
            header = this._headers[key];
        }
        return header;
    };

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.core.security.authenticator'));

    beforeEach(inject(function ($rootScope, _authenticator_, _$location_, _$cookieStore_) {
        authenticator = _authenticator_;
        $location = _$location_;
        $cookieStore = _$cookieStore_;

        // clean cookie if exists
        $cookieStore.remove(tokenId);
        $cookieStore.remove(wrongTokenId);
    }));

    it('should store token to cookie when header exists', function () {

        authenticator.storeToken(new Response({
            'Auth-Token': 'uuid 01'
        }));

        expect($cookieStore.get(tokenId)).toBe('uuid 01');
    });

    it('should not store token to cookie when header not exists', function () {

        authenticator.storeToken(new Response({
            'Token': 'uuid 02'
        }));

        expect($cookieStore.get(tokenId)).toBeUndefined();
    });

    it('should return token value when exists', function () {

        $cookieStore.put(tokenId, 'token');

        expect(authenticator.token()).toBe('token');
    });

    it('should redirect to "edit" when after redirect to "login" from "edit"', function () {

        $location.path('/edit');
        authenticator.redirectToLogin();

        expect($location.path()).toEqual('/login');

        authenticator.maybeRedirect();

        expect($location.path()).toEqual('/edit');
    });

    it('should redirect to "list" when after redirect from scratch', function () {

        authenticator.maybeRedirect();

        expect($location.path()).toEqual('/list');
    });

    it('should destroy cookie when logout', function () {

        $cookieStore.put(tokenId, 'token');

        authenticator.logout();

        expect($cookieStore.get(tokenId)).toBeUndefined();
    });

    it('should redirect to "list" when logout', function () {

        $cookieStore.put(tokenId, 'token');

        authenticator.logout();
        authenticator.maybeRedirect();

        expect($location.path()).toEqual('/list');
    });

});
