(function () {
    'use strict';

    angular
        .module('zenContact.core.security.authenticator', [
            'ngCookies'
        ])
        .factory('authenticator', ['$location', '$cookieStore',
            function ($location, $cookieStore) {

                var redirectUrl;
                return {
                    maybeRedirect: function () {
                        $location.path(redirectUrl ? redirectUrl : '/list');
                    },
                    logout: function () {
                        redirectUrl = null;
                        $cookieStore.remove('Auth-Token');
                    },
                    token: function () {
                        return $cookieStore.get('Auth-Token');
                    },
                    redirectToLogin: function () {
                        redirectUrl = $location.path();
                        $location.path('/login');
                    },
                    storeToken: function (response) {
                        var token = response.headers('Auth-Token');
                        if (token) {
                            $cookieStore.put('Auth-Token', token);
                        }
                    }
                };
            }
        ]);
})();
