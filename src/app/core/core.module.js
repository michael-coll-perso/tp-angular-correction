(function () {
    'use strict';

    angular.module('zenContact.core', [
        'zenContact.core.contact',
        'zenContact.core.security'
    ]);

})();
